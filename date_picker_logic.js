var arr = ["צפייה בכוכבים","סרט", "פיקניק בפארק השכונתי", "ערב משחקי קופסא","אימון זוגי","לבשל ארוחה ממסעדה שאוהבים", "לצפות בשקיעה בים",
    ";) ספא ביתי כולל מאסז'ים", "שחזור דייט ראשון", "ערב קוקטיילים", "משחקי וידיאו", "קריוקי", "פאזל", "ארוחה מסביב לעולם", "רכיבה על אופניים", "ערב טעימות יין", "משחק 20 השאלות"];
// Get the modal
var modal = document.getElementById("id01");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");
var again_btn = document.getElementsByClassName("again_btn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
    shuffle(arr);
    document.getElementById("myText").innerHTML = arr[0];
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

function myFunction() {
    shuffle(arr);
    document.getElementById("myText").innerHTML = arr[0];
}

function shuffle(array) {
    array.sort(() => Math.random() - 0.5);
}